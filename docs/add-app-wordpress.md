### How to add a wordpress app to Kube

Before you start, check out the [How do I add my app to Kube?](/add-app-to-kube) section of the documentation. It will give you a standard overview on how to set up the project.

Here is an [example configuration](https://bitbucket.org/colabcoop-ondemand/oaklandfutures/src/production/.deploy/) for a Wordpress Website which you can check out.

If you use the above example as a blueprint, we can look at some specific details about the configuration.

The directory structure should looks as follows:

    - [project-name]-app
	- [project-name]-db
	- kubefile.yaml
	
The application configuration should be placed in the [project-name]-app folder, the database should be placed in a [project-name]-db folder.

*kubefile.yaml* contains the configuration of the deployment.

### [project-name]-app folder

#### kubefile.yaml

The kubefile sets the foundation for the deployment. It sets up the application within the cluster and creates third party services as the database.

    namespace: example # the namespace of the project. In this case, it will be "example"
	deployments: # multiple deployments will be done for this project. We need an app container and a DB container.
	  - name: example-app # the name of the app
		chart: colab/container # the container chart
		class: small # class can be small, medium, large. Most project fit the medium package
		secrets: # the secrets that are located in our Vault
		  - vault: urmc-app
			path: secrets
		  - vault: urmc-ci
			path: ci
	  - name: urmc-db # the database container
		chart: bitnami/mysql # we are using the bitnami mysql chart
		class: medium # the package of the container
		secrets: # secrets of the database
		  - vault: urmc-db # vault account
			path: auth
			
#### Dockerfile

This is an example wordpress dockerfile. It will install all the libraries and composer.
	
	FROM php:7.4-fpm
	
	RUN apt-get update
	
	RUN apt-get install -y nginx git unzip supervisor rsync nano wget curl
	
	RUN apt-get install -y \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libxml2-dev \
		libonig-dev \
		libpng-dev
	
	RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
	RUN docker-php-ext-configure gd --with-freetype --with-jpeg
	RUN docker-php-ext-install -j$(nproc) gd
	RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath
	RUN docker-php-ext-install opcache
	
	RUN apt-get install -y libmagickwand-dev --no-install-recommends
	
	COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
	
	RUN curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash
	
	RUN pecl install imagick
	RUN docker-php-ext-enable imagick
	
	RUN printf "\n" | pecl install apcu
	RUN docker-php-ext-enable apcu
	
	COPY .deploy/of-app/nginx.conf /etc/nginx/nginx.conf
	COPY .deploy/of-app/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
	
	ENV APP_HOME /app
	RUN mkdir $APP_HOME
	WORKDIR $APP_HOME
	COPY . /app
	
	COPY .deploy/of-app/wp-config.kube.php /app/web/wp-config.php
	
	ENTRYPOINT /app/.deploy/of-app/entrypoint.sh
	

#### entrypoint.sh

The entrypoint.sh file gets run every time you run the container. It spawns a supervisor instance and runs the application.

    #!/bin/sh
	
	set -e
	
	mkdir -p $FILES_PATH
	
	rm -rf /app/web/wp-content
	ln -s $FILES_PATH /app/web/wp-content
	
	if [ ! -f $FILES_PATH/../deploy.lock ]
	then
	
	  touch $FILES_PATH/../deploy.lock
	
	  cp -r /app/themes/Divi-child/ $FILES_PATH/themes/Divi-child/
	
	  chown www-data:root $FILES_PATH/ -R
	  
	fi
	
	rm -rf $FILES_PATH/../deploy.lock
	
	/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
	




