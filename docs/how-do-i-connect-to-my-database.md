#### How do I connect to my database?

The Kube is managed with some tools which are available to access different kinds of databases.

Currently, we support:

- PgAdmin
- PhpMyAdmin

In order to access these tools, you need your credentials. The credentials are available in **Vault**. You can find more information about it [here](application-credentials.md).

Each tool has its own entry in **Vault** where you can see your application credentials.

