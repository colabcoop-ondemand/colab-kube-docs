# Glossary

Cluster - A set of nodes (worker machines). Every cluster has at least one worker node. 

Container - Virtual machine with shared OS and other tools. Each container has its own filesystem, share of CPU, memory, process space, and other features. Best practices call for each container to run a single application.

Docker - A system that enables applications to run inside containers. ([Documentation](https://docs.docker.com/))

Kube - CoLab’s implementation of Kubernetes.

Kubernetes - An open-source system for automating deployment, scaling, and management of applications in Dock containers. ([Documentation](https://kubernetes.io/))

Namespace - A group of virtual servers running on the Kube.

Node - A worker machine (server) that contains pods running containerized applications. Nodes run in the Kube cluster.

Pod - A set of containers running in a node. Kube can replicate pods when demand is high.

