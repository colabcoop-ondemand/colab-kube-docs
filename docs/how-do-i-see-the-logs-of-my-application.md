### How do I see the logs of my application?

You can check out the logs of your application using the **Kubernetes dashboard**.

To log in to the **Kubernetes dashboard** you will need your credentials which are located in the **Vault**. You can find more information about it [here](application-credentials.md).

The dashboard can be located **here**:

 - [Production environment](https://dashboard.v2.kube.colab.coop/)
 - [Staging environment](https://dashboard.v2.kube.stage.colab.coop/)
 
 In the **Kubernetes dashboard** there is a **Pods** section where you can select the Pod you'd like to inspect. Once you select the Pod, look at the top right section where you can see a :material-text-long: icon. 