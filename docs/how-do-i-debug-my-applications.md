#### How do I debug my applications?

You can debug your applications using the **Kubernetes dashboard**.

To log in to the **Kubernetes dashboard** you will need your credentials which are located in the **Vault**. You can find more information about it [here](application-credentials.md).

The dashboard can be located **here**:

 - [Production environment](https://dashboard.v2.kube.colab.coop/)
 - [Staging environment](https://dashboard.v2.kube.stage.colab.coop/)
 
 When you are logged in select the project you'd like to debug in the top dropdown. Once done, select the **Pods** item in the right menu. Within the **Pods** section, you can choose which Pod you'd like to debug. When you are in the Pod, click the :material-import: icon in the top right section.