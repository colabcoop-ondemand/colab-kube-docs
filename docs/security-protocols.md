### Kube Security Protocols

CoLab takes the security of our Kube infrastructure seriously. Here are the protocols and systems we use to keep Kube security tight.

#### Access restrictions
CoLab Kube limits who has access to virtual machines and restricts physical access to servers. Ignoring data security makes it easy for hackers to compromise servers and the information they’re able to see. We require Secure Socket Shell (SSH) or an equivalent network protocol for login access.

We provide:
- A robust password authentication system using strong encryption standards
- Authentication with public keys
- Encrypted data communication for remote system and application management
- Encryption in transit and at rest
- Role-based access control

CoLab Kube users are given access to resources on an as-needed and least-privilege principle basis. Network access is segregated to specific namespaces / users.

#### Credentials management
All credentials and secrets are managed by HashiCorp Vault. Vault provides a secure store and tight control access to tokens, passwords and certificates, with encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API, and secrets rotation support.

#### Data backups and easy rebuilds

You shouldn’t have to lose all your data or build from scratch in the event your website crashes or is hacked. Ideally, web hosts should provide two types of backups: physical and digital.
A physical backup should exist at a second location if a server location is compromised. You should create a digital backup of your website to restore an earlier version if something goes wrong.

We provide:
- A hosting package that includes automatic backups
- Frequent cluster level backups (daily)
- Frequent namespace level backups (3x per day)
- Backup storage for 30 days, so you have 90 backups available in case you need to restore

#### Managed hosting service

CoLab Kube provides a managed hosting environment, which provides a higher level of security since fewer sites use server resources, and specific security measures are implemented for every site.

Our hosting environment provides:
- Namespace isolation
- Process monitoring using metric analysis services (Prometheus)
- Efficient resource usage

#### System upgrades / updates

CoLab Kube is regularly upgraded and updated to the latest version of the underlying infrastructure. We keep things up to date to ensure that our systems are secure and to prevent potential vulnerabilities.

#### Redundant storage

CoLab Kube manages storage using a networked file system called CEPH. CEPH also offers high-performance, S3-compatible object storage. With storing the duplicate PGs on at least 3 nodes at the same time, it provides enough redundancy so that the system can run even if there are problems with the bare metal storage or file system nodes.

#### Versioned code

CoLab Kube deploys code using git-based repositories. This means we can easily roll back if needed to a previous version.

#### Security incident response

The CoLab Kube team uses an alerting system called OpsGenie. If a system/security incident happens, the relevant on-call people are alerted and follow procedures to identify and close any security hole and recover any compromised systems back to the previous state as soon as possible. In addition to that we also keep track of Kubernetes, CoLab Operator and deployment events via internal communication channels. 

#### CoLab Operator

In-house built set of scripts being invoked regularly, which checks and alerts us in cases of failed backups, certificate renewals, it also does ingress auto discovery, with automated setting up NodePing alerts.

#### Server incident response

The CoLab Kube team uses an alerting system called OpsGenie. If a system incident happens, the relevant on-call people are alerted and follow procedures to identify and close any security hole and recover any compromised systems back to the previous state as soon as possible.
After the incident is resolved, a post-mortem report is written and communicated to the client.

#### Human factors and customer responsibilities

CoLab IT staff are trained on secure software practices and can provide training to customer staff. 
Customers are responsible for:
- Securing passwords and private keys
- Secure application configuration and code

#### Audits

All our systems are audited using monitoring software, such as:
- Kibana (infrastructure logging, server logs, app logs)
- Grafana (pricing)

We collect data on a container level, credentials level and overall systems level.
