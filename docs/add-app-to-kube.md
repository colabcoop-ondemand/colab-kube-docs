## Preparing the project

In order to add your project to KUBE, you will have to do some simple steps. Here is a breakdown of what needs to be done before you will be able to deploy the app to KUBE.

### General concepts

Each project has a `.deploy` directory. The directory contains the following files:

- files [folder where we put in container-specific configs like nginx, supervisor]
- values [folder with db, env, prod, staging env variables]
- Dockerfile [the app Dockerfile]
- entrypoint.sh [the script that runs when the docker container is ready]
- helmfile.yaml

#### Dockerfile

If you have an existing Dockerfile, you can use that. If you need a sample one, here is one:

````
FROM python:3.9

RUN apt-get update

RUN apt-get install -y nginx git unzip supervisor rsync nano wget curl cron

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install 'Django>=2.0,<3.0'
RUN pip install 'psycopg2>=2.7,<3.0'
RUN pip install 'djangorestframework>=3.10,<4.0'
RUN pip install 'Pillow>=6.2.1,<7.0'
RUN pip install 'django-haystack>=2.8.1,<3.0'
RUN pip install 'whoosh>=2.7.4,<3.0'
RUN pip install 'django-environ>=0.4.5,<0.5'
RUN pip install 'django-admin-sortable2>=0.7.5,<0.8'
RUN pip install 'django-ckeditor>=5.9.0,<6.0'
RUN pip install 'django-debug-toolbar==2.2'
RUN pip install 'easy-thumbnails==2.7'
RUN pip install 'gunicorn==20.0.4'

COPY .deploy/files/cron.conf /etc/cron.d/cron
COPY .deploy/files/nginx.conf /etc/nginx/nginx.conf
COPY .deploy/files/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN chmod 0644 /etc/cron.d/cron
RUN crontab /etc/cron.d/cron
RUN touch /var/log/cron.log

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
COPY . /app

ENTRYPOINT /app/.deploy/entrypoint.sh
````

#### values folder

The values folder contains all the environment variables. For example, [this is a database yaml env file]() for postgres.

#### files folder

The files folder contains all the relevant config files for nginx, etc.

#### entrypoint.sh

The entrypoint.sh is a script that runs when the container is ready.

#### helmfile.yaml

Helmfile is a declarative spec for deploying Helm charts. It lets you:

- Keep a directory of chart value files and maintain changes in version control.
- Apply CI/CD to configuration changes.
- Environmental chart promotion.
- Periodically sync to avoid skew in environments.
