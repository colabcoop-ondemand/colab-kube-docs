#### Application credentials

All the credentials or application secrets needed to access the Kube are available in **Vault**.

You can access the vault URL from within 1Password. You will see the namespaces you have access to and the secrets used in that namespace. These secrets are used by the deploy script for the application, including any credentials for services or that are needed in environment variables.

You can access the **Vault** here:

- [Production environment](https://vault.v2.kube.colab.coop/)
- [Staging environment](https://vault.v2.kube.stage.colab.coop/)